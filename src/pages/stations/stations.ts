import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../base/base';
import { StationProvider as Station } from '../../providers/station/station';

@IonicPage({
  name: 'stations',
  segment: 'stations'
})
@Component({
  selector: 'page-stations',
  templateUrl: 'stations.html'
})
export class StationsPage extends BasePage {
  params: any = {};
  stations: Station[];
  field: string = 'name';
  direction: string = '';

  constructor(public injector: Injector) {
    super(injector);
  }

  ionViewDidLoad() {
    this.showLoadingView('Loading...');
    this.onReload();
  }

  onReload(refresher?: any) {
    this.refresher = refresher;

    this.params.sortBy = `${this.direction}${this.field}`;
    this.params.page = 1;
    this.stations = [];

    this.loadData();
  }

  async loadData() {
    try {
      let data = await Station.load(this.params);
      this.stations = this.stations.concat(data);
      this.onRefreshComplete(data);
      if (this.stations.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.onRefreshComplete();
      this.showErrorView();
    }
  }

  onSearch() {
    this.showLoadingView('Searching...');
    this.onReload();
  }

  onClearSearch() {
    this.params.search = '';
    this.ionViewDidLoad();
  }

  onLoadMore(infiniteScroll: any) {
    this.infiniteScroll = infiniteScroll;
    this.params.page++;
    this.loadData();
  }
}
