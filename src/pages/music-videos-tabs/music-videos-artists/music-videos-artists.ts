import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { MusicVideoProvider as MusicVideo } from '../../../providers/music-video/music-video';

@IonicPage({
  segment: 'music-videos/artists'
})
@Component({
  selector: 'page-music-videos-artists',
  templateUrl: 'music-videos-artists.html'
})
export class MusicVideosArtistsPage extends BasePage {
  params: any = { field: 'artist' };
  artists: any[];
  field: string = 'artist';
  direction: string = '1';

  constructor(public injector: Injector) {
    super(injector);
  }

  ionViewDidLoad() {
    this.showLoadingView('Loading...');
    this.onReload();
  }

  onReload(refresher?: any) {
    this.refresher = refresher;

    this.params.sort = this.direction;
    this.params.page = 1;
    this.artists = [];

    this.loadData();
  }

  async loadData() {
    try {
      let data = await MusicVideo.listOf(this.params);
      this.artists = this.artists.concat(data);
      this.onRefreshComplete(data);
      if (this.artists.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.onRefreshComplete();
      this.showErrorView();
    }
  }

  onSearch() {
    this.showLoadingView('Searching...');
    this.onReload();
  }

  onClearSearch() {
    this.params.search = '';
    this.ionViewDidLoad();
  }

  onLoadMore(infiniteScroll: any) {
    this.infiniteScroll = infiniteScroll;
    this.params.page++;
    this.loadData();
  }
}
