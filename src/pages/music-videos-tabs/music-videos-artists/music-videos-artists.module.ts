import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicVideosArtistsPage } from './music-videos-artists';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [MusicVideosArtistsPage],
  imports: [IonicPageModule.forChild(MusicVideosArtistsPage), ComponentsModule]
})
export class MusicVideosArtistsPageModule {}
