import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { MusicVideoProvider as MusicVideo } from '../../../providers/music-video/music-video';

@IonicPage({
  segment: 'music-videos/genres'
})
@Component({
  selector: 'page-music-videos-genres',
  templateUrl: 'music-videos-genres.html'
})
export class MusicVideosGenresPage extends BasePage {
  params: any = { field: 'genre' };
  genres: any[];
  field: string = 'genre';
  direction: string = '1';

  constructor(public injector: Injector) {
    super(injector);
  }

  ionViewDidLoad() {
    this.showLoadingView('Loading...');
    this.onReload();
  }

  onReload(refresher?: any) {
    this.refresher = refresher;

    this.params.sort = this.direction;
    this.params.page = 1;
    this.genres = [];

    this.loadData();
  }

  async loadData() {
    try {
      let data = await MusicVideo.listOf(this.params);
      this.genres = this.genres.concat(data);
      this.onRefreshComplete(data);
      if (this.genres.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.onRefreshComplete();
      this.showErrorView();
    }
  }

  onSearch() {
    this.showLoadingView('Searching...');
    this.onReload();
  }

  onClearSearch() {
    this.params.search = '';
    this.ionViewDidLoad();
  }

  onLoadMore(infiniteScroll: any) {
    this.infiniteScroll = infiniteScroll;
    this.params.page++;
    this.loadData();
  }
}
