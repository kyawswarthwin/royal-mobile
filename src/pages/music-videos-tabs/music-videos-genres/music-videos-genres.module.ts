import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicVideosGenresPage } from './music-videos-genres';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [MusicVideosGenresPage],
  imports: [IonicPageModule.forChild(MusicVideosGenresPage), ComponentsModule]
})
export class MusicVideosGenresPageModule {}
