import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { MusicVideoProvider as MusicVideo } from '../../../providers/music-video/music-video';

@IonicPage({
  segment: 'music-videos/songs'
})
@Component({
  selector: 'page-music-videos-songs',
  templateUrl: 'music-videos-songs.html'
})
export class MusicVideosSongsPage extends BasePage {
  params: any = {};
  songs: MusicVideo[];
  field: string = 'updatedAt';
  direction: string = '-';

  constructor(public injector: Injector) {
    super(injector);

    this.params = { ...this.params, ...this.navParams.data };
    if (this.params.sortBy) {
      if (this.params.sortBy.charAt(0) === '-') {
        this.field = this.params.sortBy.substr(1);
        this.direction = '-';
      } else {
        this.field = this.params.sortBy;
        this.direction = '';
      }
    }
  }

  ionViewDidLoad() {
    this.showLoadingView('Loading...');
    this.onReload();
  }

  onReload(refresher?: any) {
    this.refresher = refresher;

    this.params.sortBy = `${this.direction}${this.field}`;
    this.params.page = 1;
    this.songs = [];

    this.loadData();
  }

  async loadData() {
    try {
      let data = await MusicVideo.load(this.params);
      this.songs = this.songs.concat(data);
      this.onRefreshComplete(data);
      if (this.songs.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.onRefreshComplete();
      this.showErrorView();
    }
  }

  onSearch() {
    this.showLoadingView('Searching...');
    this.onReload();
  }

  onClearSearch() {
    this.params.search = '';
    this.ionViewDidLoad();
  }

  onLoadMore(infiniteScroll: any) {
    this.infiniteScroll = infiniteScroll;
    this.params.page++;
    this.loadData();
  }
}
