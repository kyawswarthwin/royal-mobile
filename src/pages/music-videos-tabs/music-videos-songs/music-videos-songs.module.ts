import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicVideosSongsPage } from './music-videos-songs';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [MusicVideosSongsPage],
  imports: [IonicPageModule.forChild(MusicVideosSongsPage), ComponentsModule]
})
export class MusicVideosSongsPageModule {}
