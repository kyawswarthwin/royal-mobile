import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicVideosAlbumsPage } from './music-videos-albums';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [MusicVideosAlbumsPage],
  imports: [IonicPageModule.forChild(MusicVideosAlbumsPage), ComponentsModule]
})
export class MusicVideosAlbumsPageModule {}
