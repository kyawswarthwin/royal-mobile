import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicVideosTabsPage } from './music-videos-tabs';

@NgModule({
  declarations: [
    MusicVideosTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(MusicVideosTabsPage),
  ],
})
export class MusicVideosTabsPageModule {}
