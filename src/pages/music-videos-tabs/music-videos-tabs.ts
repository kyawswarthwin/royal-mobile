import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage({
  name: 'music-videos',
  segment: 'music-videos'
})
@Component({
  selector: 'page-music-videos-tabs',
  templateUrl: 'music-videos-tabs.html'
})
export class MusicVideosTabsPage {
  tab1Root: any = 'MusicVideosSongsPage';
  tab2Root: any = 'MusicVideosArtistsPage';
  tab3Root: any = 'MusicVideosAlbumsPage';
  tab4Root: any = 'MusicVideosGenresPage';

  constructor() {}
}
