import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsEventsTabsPage } from './news-events-tabs';

@NgModule({
  declarations: [
    NewsEventsTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(NewsEventsTabsPage),
  ],
})
export class NewsEventsTabsPageModule {}
