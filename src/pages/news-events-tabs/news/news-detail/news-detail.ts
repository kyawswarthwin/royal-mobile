import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../../base/base';
import { FacebookProvider } from '../../../../providers/facebook/facebook';

@IonicPage({
  segment: 'news-events/news/:id'
})
@Component({
  selector: 'page-news-detail',
  templateUrl: 'news-detail.html'
})
export class NewsDetailPage extends BasePage {
  post: any = {};

  constructor(public injector: Injector, public fb: FacebookProvider) {
    super(injector);
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      this.post = await this.fb.getPosts({
        id: this.navParams.data.id
      });
      if (this.post) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.showErrorView();
    }
  }
}
