import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { FacebookProvider } from '../../../providers/facebook/facebook';

@IonicPage({
  segment: 'news-events/news'
})
@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class NewsPage extends BasePage {
  params: any = {};
  posts: any[];

  constructor(public injector: Injector, public fb: FacebookProvider) {
    super(injector);
  }

  ionViewDidLoad() {
    this.showLoadingView('Loading...');
    this.onReload();
  }

  onReload(refresher?: any) {
    this.refresher = refresher;

    this.params = {};
    this.posts = [];

    this.loadData();
  }

  async loadData() {
    try {
      let data = await this.fb.getPosts(this.params);
      this.posts = this.posts.concat(data.data);
      this.params.after = data.paging && data.paging.cursors.after;
      this.onRefreshComplete(data.data);
      if (this.posts.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.onRefreshComplete();
      this.showErrorView();
    }
  }

  onLoadMore(infiniteScroll: any) {
    this.infiniteScroll = infiniteScroll;
    this.loadData();
  }
}
