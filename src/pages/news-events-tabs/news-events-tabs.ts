import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage({
  name: 'news-events',
  segment: 'news-events'
})
@Component({
  selector: 'page-news-events-tabs',
  templateUrl: 'news-events-tabs.html'
})
export class NewsEventsTabsPage {
  tab1Root: any = 'NewsPage';
  tab2Root: any = 'EventsPage';

  constructor() {}
}
