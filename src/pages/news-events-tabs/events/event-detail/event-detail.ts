import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../../base/base';
import { FacebookProvider } from '../../../../providers/facebook/facebook';

@IonicPage({
  segment: 'news-events/events/:id'
})
@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html'
})
export class EventDetailPage extends BasePage {
  event: any = {};

  constructor(public injector: Injector, public fb: FacebookProvider) {
    super(injector);
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      this.event = await this.fb.getEvents({
        id: this.navParams.data.id
      });
      if (this.event) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.showErrorView();
    }
  }
}
