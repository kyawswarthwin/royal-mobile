import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsPage } from './events';
import { ComponentsModule } from '../../../components/components.module';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  declarations: [EventsPage],
  imports: [IonicPageModule.forChild(EventsPage), ComponentsModule, PipesModule]
})
export class EventsPageModule {}
