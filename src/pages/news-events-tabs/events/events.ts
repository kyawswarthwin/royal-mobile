import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { FacebookProvider } from '../../../providers/facebook/facebook';

@IonicPage({
  segment: 'news-events/events'
})
@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage extends BasePage {
  params: any = {};
  events: any[];

  constructor(public injector: Injector, public fb: FacebookProvider) {
    super(injector);
  }

  ionViewDidLoad() {
    this.showLoadingView('Loading...');
    this.onReload();
  }

  onReload(refresher?: any) {
    this.refresher = refresher;

    this.params = {};
    this.events = [];

    this.loadData();
  }

  async loadData() {
    try {
      let data = await this.fb.getEvents(this.params);
      this.events = this.events.concat(data.data);
      this.params.after = data.paging && data.paging.cursors.after;
      this.onRefreshComplete(data.data);
      if (this.events.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.onRefreshComplete();
      this.showErrorView();
    }
  }

  onLoadMore(infiniteScroll: any) {
    this.infiniteScroll = infiniteScroll;
    this.loadData();
  }
}
