import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../../base/base';
import { SerieProvider as Serie } from '../../../../providers/serie/serie';

@IonicPage({
  segment: 'series/series/:show'
})
@Component({
  selector: 'page-serie-detail',
  templateUrl: 'serie-detail.html'
})
export class SerieDetailPage extends BasePage {
  show: string;
  seasons: any[];

  constructor(public injector: Injector) {
    super(injector);

    this.show = this.navParams.data.show;
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      this.seasons = await Serie.detail({ show: this.show });
      if (this.seasons.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.showErrorView();
    }
  }
}
