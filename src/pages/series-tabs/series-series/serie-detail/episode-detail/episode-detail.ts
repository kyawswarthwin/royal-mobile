import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../../../base/base';
import { SerieProvider as Serie } from '../../../../../providers/serie/serie';

@IonicPage({
  segment: 'series/series/:id'
})
@Component({
  selector: 'page-episode-detail',
  templateUrl: 'episode-detail.html'
})
export class EpisodeDetailPage extends BasePage {
  episode: Serie;

  constructor(public injector: Injector) {
    super(injector);

    this.episode = new Serie();
    this.episode.id = this.navParams.data.id;
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      await this.episode.fetch();
      this.showContentView();
    } catch (error) {
      if (error.code === 101) {
        this.showEmptyView();
      } else {
        this.showErrorView();
      }
    }
  }
}
