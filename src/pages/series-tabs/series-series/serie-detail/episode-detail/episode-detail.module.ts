import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EpisodeDetailPage } from './episode-detail';
import { ComponentsModule } from '../../../../../components/components.module';
import { PipesModule } from '../../../../../pipes/pipes.module';

@NgModule({
  declarations: [EpisodeDetailPage],
  imports: [IonicPageModule.forChild(EpisodeDetailPage), ComponentsModule, PipesModule]
})
export class EpisodeDetailPageModule {}
