import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../../base/base';
import { MovieProvider as Movie } from '../../../../providers/movie/movie';

@IonicPage({
  segment: 'movies/movies/:id'
})
@Component({
  selector: 'page-movie-detail',
  templateUrl: 'movie-detail.html'
})
export class MovieDetailPage extends BasePage {
  movie: Movie;

  constructor(public injector: Injector) {
    super(injector);

    this.movie = new Movie();
    this.movie.id = this.navParams.data.id;
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      await this.movie.fetch();
      this.showContentView();
    } catch (error) {
      if (error.code === 101) {
        this.showEmptyView();
      } else {
        this.showErrorView();
      }
    }
  }
}
