import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MovieDetailPage } from './movie-detail';
import { ComponentsModule } from '../../../../components/components.module';
import { PipesModule } from '../../../../pipes/pipes.module';

@NgModule({
  declarations: [MovieDetailPage],
  imports: [IonicPageModule.forChild(MovieDetailPage), ComponentsModule, PipesModule]
})
export class MovieDetailPageModule {}
