import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../base/base';
import { GameProvider as Game } from '../../providers/game/game';

@IonicPage({
  name: 'games',
  segment: 'games'
})
@Component({
  selector: 'page-games',
  templateUrl: 'games.html'
})
export class GamesPage extends BasePage {
  params: any = {};
  games: Game[];
  field: string = 'updatedAt';
  direction: string = '-';

  constructor(public injector: Injector) {
    super(injector);
  }

  ionViewDidLoad() {
    this.showLoadingView('Loading...');
    this.onReload();
  }

  onReload(refresher?: any) {
    this.refresher = refresher;

    this.params.sortBy = `${this.direction}${this.field}`;
    this.params.page = 1;
    this.games = [];

    this.loadData();
  }

  async loadData() {
    try {
      let data = await Game.load(this.params);
      this.games = this.games.concat(data);
      this.onRefreshComplete(data);
      if (this.games.length) {
        this.showContentView();
      } else {
        this.showEmptyView();
      }
    } catch (error) {
      this.onRefreshComplete();
      this.showErrorView();
    }
  }

  onSearch() {
    this.showLoadingView('Searching...');
    this.onReload();
  }

  onClearSearch() {
    this.params.search = '';
    this.ionViewDidLoad();
  }

  onLoadMore(infiniteScroll: any) {
    this.infiniteScroll = infiniteScroll;
    this.params.page++;
    this.loadData();
  }
}
