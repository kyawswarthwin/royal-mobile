import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { StationProvider as Station } from '../../../providers/station/station';

@IonicPage({
  segment: 'stations/:id',
  defaultHistory: ['stations']
})
@Component({
  selector: 'page-radio-player',
  templateUrl: 'radio-player.html'
})
export class RadioPlayerPage extends BasePage {
  station: Station;

  constructor(public injector: Injector) {
    super(injector);

    this.station = new Station();
    this.station.id = this.navParams.data.id;
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      await this.station.fetch();
      this.showContentView();
    } catch (error) {
      if (error.code === 101) {
        this.showEmptyView();
      } else {
        this.showErrorView();
      }
    }
  }
}
