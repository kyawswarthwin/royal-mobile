import { Component, Injector, ViewChild } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { MovieProvider as Movie } from '../../../providers/movie/movie';
import { SerieProvider as Serie } from '../../../providers/serie/serie';
import { MusicVideoProvider as MusicVideo } from '../../../providers/music-video/music-video';

@IonicPage({
  segment: ':type/:type/:id/play/:isAlbum'
})
@Component({
  selector: 'page-video-player',
  templateUrl: 'video-player.html'
})
export class VideoPlayerPage extends BasePage {
  @ViewChild('video') video: any;

  type: string;
  movie: any;
  isAlbum: boolean;
  tracks: any[];
  playlist: any[] = [];
  startIndex: number;
  player: any;
  isPaused: boolean;

  constructor(public injector: Injector) {
    super(injector);

    this.type = this.navParams.data.type;
    switch (this.type) {
      case 'movies':
        this.movie = new Movie();
        break;
      case 'series':
        this.movie = new Serie();
        break;
      case 'music-videos':
        this.movie = new MusicVideo();
        break;
      default:
        break;
    }
    this.movie.id = this.navParams.data.id;
    this.isAlbum = this.navParams.data.isAlbum;
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      await this.movie.fetch();
      if (this.isAlbum) {
        this.tracks = await MusicVideo.load({
          by: 'album',
          value: this.movie.album,
          sortBy: 'track'
        });
        this.startIndex = this.tracks.findIndex(data => data.id === this.movie.id);
      }
      this.playlist = this.playlist.concat(this.tracks || this.movie).map(movie => {
        return {
          sources: [
            {
              src: this.getMediaUrl(movie.file)
            }
          ]
        };
      });
      this.showContentView();
    } catch (error) {
      if (error.code === 101) {
        this.showEmptyView();
      } else {
        this.showErrorView();
      }
    }
  }

  ionViewDidEnter() {
    this.player = this.video.player;
    this.player.on(['loadstart', 'playlistchange', 'playlistsorted'], event => {
      this.movie = this.tracks[this.player.playlist.currentItem()];
    });
    this.player.on('play', event => {
      this.isPaused = false;
    });
    this.player.on('pause', event => {
      this.isPaused = true;
    });
  }
}
