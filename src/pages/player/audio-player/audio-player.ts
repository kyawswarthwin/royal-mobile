import { Component, Injector, ViewChild } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { MusicProvider as Music } from '../../../providers/music/music';

@IonicPage({
  segment: 'music/songs/:id/:isAlbum'
})
@Component({
  selector: 'page-audio-player',
  templateUrl: 'audio-player.html'
})
export class AudioPlayerPage extends BasePage {
  @ViewChild('audio') audio: any;

  song: Music;
  isAlbum: boolean;
  tracks: Music[];
  playlist: any[] = [];
  startIndex: number;
  player: any;
  isPaused: boolean;

  constructor(public injector: Injector) {
    super(injector);

    this.song = new Music();
    this.song.id = this.navParams.data.id;
    this.isAlbum = this.navParams.data.isAlbum;
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      await this.song.fetch();
      if (this.isAlbum) {
        this.tracks = await Music.load({
          by: 'album',
          value: this.song.album,
          sortBy: 'track'
        });
        this.startIndex = this.tracks.findIndex(data => data.id === this.song.id);
      }
      this.playlist = this.playlist.concat(this.tracks || this.song).map(song => {
        return {
          sources: [
            {
              src: this.getMediaUrl(song.file)
            }
          ]
        };
      });
      this.showContentView();
    } catch (error) {
      if (error.code === 101) {
        this.showEmptyView();
      } else {
        this.showErrorView();
      }
    }
  }

  ionViewDidEnter() {
    this.player = this.audio.player;
    this.player.on(['loadstart', 'playlistchange', 'playlistsorted'], event => {
      this.song = this.tracks[this.player.playlist.currentItem()];
    });
    this.player.on('play', event => {
      this.isPaused = false;
    });
    this.player.on('pause', event => {
      this.isPaused = true;
    });
  }
}
