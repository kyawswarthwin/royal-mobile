import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { BasePage } from '../../base/base';
import { ChannelProvider as Channel } from '../../../providers/channel/channel';

@IonicPage({
  segment: 'channels/:id',
  defaultHistory: ['channels']
})
@Component({
  selector: 'page-tv-player',
  templateUrl: 'tv-player.html'
})
export class TvPlayerPage extends BasePage {
  channel: Channel;

  constructor(public injector: Injector) {
    super(injector);

    this.channel = new Channel();
    this.channel.id = this.navParams.data.id;
  }

  async ionViewDidLoad() {
    try {
      this.showLoadingView('Loading...');
      await this.channel.fetch();
      this.showContentView();
    } catch (error) {
      if (error.code === 101) {
        this.showEmptyView();
      } else {
        this.showErrorView();
      }
    }
  }
}
