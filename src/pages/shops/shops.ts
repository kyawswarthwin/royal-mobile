import { Component, Injector, NgZone } from '@angular/core';
import { IonicPage, ActionSheetController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { filter } from 'rxjs/operators';
import geolib from 'geolib';

import { BasePage } from '../base/base';

@IonicPage({
  name: 'shops',
  segment: 'shops'
})
@Component({
  selector: 'page-shops',
  templateUrl: 'shops.html'
})
export class ShopsPage extends BasePage {
  shops: any[] = [
    {
      name: 'Royal Mobile',
      address:
        'အခန္း (၁၃/၁၄)၊ အမွတ္ (၂) ရဲစခန္းတိုက္တန္း၊ ေရႊဆံေတာ္ဘုရား ေျမာက္ေစာင္းတန္းလမ္း၊ ျပည္ၿမိဳ႕။',
      latitude: 18.820578,
      longitude: 95.221179,
      phones: ['05325770', '05325046']
    }
  ];
  distances: any;
  watch: any;

  constructor(
    public injector: Injector,
    private zone: NgZone,
    private geolocation: Geolocation,
    private actionSheetCtrl: ActionSheetController
  ) {
    super(injector);
  }

  ionViewDidLoad() {
    this.watch = this.geolocation
      .watchPosition({
        enableHighAccuracy: true
      })
      .pipe(filter(position => position.coords !== undefined))
      .subscribe(position => {
        this.zone.run(() => {
          this.distances = geolib
            .orderByDistance(
              {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              },
              this.shops
            )
            .map(data => {
              data.distance = geolib.convertUnit('mi', data.distance, 2);
              return data;
            });
        });
      });
  }

  ionViewDidLeave() {
    this.watch.unsubscribe();
  }

  onClick(shop: any) {
    const buttons = [];
    shop.phones &&
      shop.phones.forEach(phone => {
        buttons.push({
          text: `Call ${phone}`,
          handler: () => {
            window.location.href = `tel:${phone}`;
          }
        });
      });
    buttons.push(
      {
        text: 'Get Directions',
        handler: () => {
          window.open(
            'https://www.google.com/maps/search/?api=1&query=' +
              shop.latitude +
              ',' +
              shop.longitude,
            '_blank'
          );
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
    );
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Action',
      buttons: buttons
    });
    actionSheet.present();
  }
}
