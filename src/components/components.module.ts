import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MessageViewComponent } from './message-view/message-view';
import { AdViewComponent } from './ad-view/ad-view';
import { VjsAudioComponent } from './vjs-audio/vjs-audio';
import { VjsVideoComponent } from './vjs-video/vjs-video';

@NgModule({
  declarations: [MessageViewComponent, AdViewComponent, VjsAudioComponent, VjsVideoComponent],
  imports: [IonicModule],
  exports: [MessageViewComponent, AdViewComponent, VjsAudioComponent, VjsVideoComponent]
})
export class ComponentsModule {}
