import { Injectable } from '@angular/core';

import Parse from 'parse';

@Injectable()
export class MovieProvider extends Parse.Object {
  constructor() {
    super('Movie');
  }

  static load(
    params?: any,
    fields: string[] = ['title', 'year', 'file']
  ): Promise<MovieProvider[]> {
    return new Promise((resolve, reject) => {
      const { by, value, search, sortBy, page, limit = 25 } = params;
      let query = new Parse.Query(this);
      // Movies By
      if (by) {
        query.equalTo(by, value);
        fields = fields.filter(data => data !== by);
      }
      // Search
      if (search) {
        let queries = [];
        fields.forEach((field, index) => {
          queries[index] = new Parse.Query(this);
          queries[index].matches(field, search, 'i');
        });
        query = Parse.Query.and(query, Parse.Query.or(...queries));
      }
      // Sort
      if (sortBy) {
        if (sortBy.charAt(0) === '-') {
          query.descending(sortBy.substr(1));
        } else {
          query.ascending(sortBy);
        }
      }
      // Paginate
      if (page >= 0) {
        query.skip((page - 1) * limit);
        query.limit(limit);
      }
      query
        .find()
        .then(resolve)
        .catch(reject);
    });
  }

  static listOf(params?: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      Parse.Cloud.run('movieListOf', params)
        .then(resolve)
        .catch(reject);
    });
  }

  get file(): string {
    return this.get('file');
  }

  get fileSize(): number {
    return this.get('fileSize');
  }

  get title(): string {
    return this.get('title');
  }

  get artist(): string[] {
    return this.get('artist');
  }

  // get album(): string {
  //   return this.get('album');
  // }

  get year(): string {
    return this.get('year');
  }

  // get track(): number {
  //   return this.get('track');
  // }

  get genre(): string[] {
    return this.get('genre');
  }

  get picture(): Parse.File {
    return this.get('picture');
  }

  get synopsis(): string {
    return this.get('synopsis');
  }
}

Parse.Object.registerSubclass('Movie', MovieProvider);
