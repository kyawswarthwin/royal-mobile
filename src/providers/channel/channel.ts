import { Injectable } from '@angular/core';

import Parse from 'parse';

@Injectable()
export class ChannelProvider extends Parse.Object {
  constructor() {
    super('Channel');
  }

  static load(params?: any, fields: string[] = ['name']): Promise<ChannelProvider[]> {
    return new Promise((resolve, reject) => {
      const { search, sortBy, page, limit = 25 } = params;
      let query = new Parse.Query(this);
      // Search
      if (search) {
        let queries = [];
        fields.forEach((field, index) => {
          queries[index] = new Parse.Query(this);
          queries[index].matches(field, search, 'i');
        });
        query = Parse.Query.and(query, Parse.Query.or(...queries));
      }
      // Sort
      if (sortBy) {
        if (sortBy.charAt(0) === '-') {
          query.descending(sortBy.substr(1));
        } else {
          query.ascending(sortBy);
        }
      }
      // Paginate
      if (page >= 0) {
        query.skip((page - 1) * limit);
        query.limit(limit);
      }
      query
        .find()
        .then(resolve)
        .catch(reject);
    });
  }

  get name(): string {
    return this.get('name');
  }

  get url(): string {
    return this.get('url');
  }
}

Parse.Object.registerSubclass('Channel', ChannelProvider);
