import { Injectable } from '@angular/core';

import Parse from 'parse';

@Injectable()
export class GameProvider extends Parse.Object {
  constructor() {
    super('Game');
  }

  static load(params?: any, fields: string[] = ['name']): Promise<GameProvider[]> {
    return new Promise((resolve, reject) => {
      const { search, sortBy, page, limit = 25 } = params;
      let query = new Parse.Query(this);
      // Search
      if (search) {
        let queries = [];
        fields.forEach((field, index) => {
          queries[index] = new Parse.Query(this);
          queries[index].matches(field, search, 'i');
        });
        query = Parse.Query.and(query, Parse.Query.or(...queries));
      }
      // Sort
      if (sortBy) {
        if (sortBy.charAt(0) === '-') {
          query.descending(sortBy.substr(1));
        } else {
          query.ascending(sortBy);
        }
      }
      // Paginate
      if (page >= 0) {
        query.skip((page - 1) * limit);
        query.limit(limit);
      }
      query
        .find()
        .then(resolve)
        .catch(reject);
    });
  }

  get file(): string {
    return this.get('file');
  }

  get fileSize(): number {
    return this.get('fileSize');
  }

  get icon(): Parse.File {
    return this.get('icon');
  }

  get name(): string {
    return this.get('name');
  }

  get uniqueIdentifier(): string {
    return this.get('uniqueIdentifier');
  }

  get version(): string {
    return this.get('version');
  }

  get buildNumber(): string {
    return this.get('buildNumber');
  }

  get minOsVersion(): string {
    return this.get('minOsVersion');
  }

  get deviceFamily(): number[] {
    return this.get('deviceFamily');
  }

  get platform(): string {
    return this.get('platform');
  }
}

Parse.Object.registerSubclass('Game', GameProvider);
