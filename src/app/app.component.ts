import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HOME_PAGE, PARSE_SERVER } from './app.config';
import Parse from 'parse';

export interface PageInterface {
  id: string;
  icon?: string;
  title: string;
}
export interface MenuInterface {
  header?: string;
  pages: PageInterface[];
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  menus: MenuInterface[] = [
    {
      pages: [
        {
          id: 'news-events',
          title: 'News & Events'
        },
        {
          id: 'shops',
          title: 'Shops'
        },
        {
          id: 'movies',
          title: 'Movies'
        },
        {
          id: 'series',
          title: 'TV Series'
        },
        {
          id: 'music',
          title: 'Music'
        },
        {
          id: 'music-videos',
          title: 'Music Videos'
        },
        {
          id: 'channels',
          title: 'TV Channels'
        },
        {
          id: 'stations',
          title: 'Radio Stations'
        },
        {
          id: 'applications',
          title: 'Applications'
        },
        {
          id: 'games',
          title: 'Games'
        }
      ]
    }
  ];

  rootPage: any = HOME_PAGE;

  constructor(private platform: Platform, private splashScreen: SplashScreen) {
    Parse.initialize(PARSE_SERVER.APP_ID);
    Parse.serverURL = PARSE_SERVER.URL;

    platform.ready().then(() => {
      splashScreen.hide();
    });
  }

  openPage(page: PageInterface) {
    this.nav.setRoot(page.id);
  }

  isActive(page: PageInterface): boolean {
    return this.nav.first() && this.nav.first().id === page.id;
  }
}
