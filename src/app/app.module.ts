import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';

import { MyApp } from './app.component';
import { MovieProvider } from '../providers/movie/movie';
import { SerieProvider } from '../providers/serie/serie';
import { MusicProvider } from '../providers/music/music';
import { MusicVideoProvider } from '../providers/music-video/music-video';
import { ChannelProvider } from '../providers/channel/channel';
import { StationProvider } from '../providers/station/station';
import { ApplicationProvider } from '../providers/application/application';
import { GameProvider } from '../providers/game/game';
import { FacebookProvider } from '../providers/facebook/facebook';

@NgModule({
  declarations: [MyApp],
  entryComponents: [MyApp],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    })
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SplashScreen,
    StatusBar,
    Geolocation,
    MovieProvider,
    SerieProvider,
    MusicProvider,
    MusicVideoProvider,
    ChannelProvider,
    StationProvider,
    ApplicationProvider,
    GameProvider,
    FacebookProvider
  ],
  bootstrap: [IonicApp]
})
export class AppModule {}
