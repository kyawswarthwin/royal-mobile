'use strict';

const Serie = require('../../app/models/Serie');

function serieLoad(request, response) {
  const { by, value, search, sortBy, page, limit = 25 } = request.params;
  const query = new Parse.Query(Serie);
  const pipeline = [{}];
  let field, direction;
  // Series By
  if (by) {
    pipeline[0]['match'] = { [`${by}`]: value };
  }
  // Search
  if (search) {
    pipeline[pipeline.length - 1]['match'] = {
      $or: [
        { show: { $regex: search, $options: 'i' } },
        { year: { $regex: search, $options: 'i' } }
      ]
    };
  }
  pipeline[pipeline.length - 1]['group'] = {
    objectId: '$show',
    year: { $min: '$year' },
    picture: { $first: '$picture' },
    updatedAt: { $max: '$_updated_at' }
  };
  // Sort
  if (sortBy) {
    if (sortBy.charAt(0) === '-') {
      field = sortBy.substr(1);
      direction = -1;
    } else {
      field = sortBy;
      direction = 1;
    }
    pipeline[pipeline.length - 1]['sort'] =
      field === 'show' ? { _id: direction } : { [`${field}`]: direction };
  }
  // Paginate
  if (page >= 0) {
    pipeline[pipeline.length - 1]['skip'] = (page - 1) * limit;
    pipeline[pipeline.length - 1]['limit'] = limit;
  }
  query
    .aggregate(pipeline)
    .then(response.success)
    .catch(response.error);
}

function serieDetail(request, response) {
  const { show } = request.params;
  const query = new Parse.Query(Serie);
  query
    .aggregate([
      {
        match: { show: show },
        sort: { season: -1, episode: 1 },
        group: {
          objectId: { $toString: `$season` },
          count: { $sum: 1 },
          episodes: {
            $push: {
              id: '$_id',
              file: '$file',
              episode: '$episode',
              episodeId: '$episodeId'
            }
          }
        }
      }
    ])
    .then(response.success)
    .catch(response.error);
}

function serieListOf(request, response) {
  const { field, search, sort, page, limit = 25 } = request.params;
  const query = new Parse.Query(Serie);
  const pipeline = [{}];
  if (field === 'artist' || field === 'genre') {
    pipeline[0]['unwind'] = {
      path: `$${field}`,
      preserveNullAndEmptyArrays: true
    };
  }
  // Search
  if (search) {
    pipeline[0]['match'] = { [`${field}`]: { $regex: search, $options: 'i' } };
  }
  pipeline[0]['group'] = {
    objectId: `$${field}`,
    count: { $sum: 1 }
  };
  // if (field === 'album') {
  //   pipeline[0]['group']['picture'] = { $first: '$picture' };
  // }
  // Sort
  if (sort) {
    pipeline[0]['sort'] = { _id: Number(sort) };
  }
  // Paginate
  if (page >= 0) {
    pipeline[0]['skip'] = (page - 1) * limit;
    pipeline[0]['limit'] = limit;
  }
  query
    .aggregate(pipeline)
    .then(response.success)
    .catch(response.error);
}

module.exports = {
  serieLoad: serieLoad,
  serieDetail: serieDetail,
  serieListOf: serieListOf
};
