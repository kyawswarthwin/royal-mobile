'use strict';

const Music = require('../../app/models/Music');

function musicListOf(request, response) {
  const { field, search, sort, page, limit = 25 } = request.params;
  const query = new Parse.Query(Music);
  const pipeline = [{}];
  if (field === 'artist' || field === 'genre') {
    pipeline[0]['unwind'] = {
      path: `$${field}`,
      preserveNullAndEmptyArrays: true
    };
  }
  // Search
  if (search) {
    pipeline[0]['match'] = { [`${field}`]: { $regex: search, $options: 'i' } };
  }
  pipeline[0]['group'] = {
    objectId: `$${field}`,
    count: { $sum: 1 }
  };
  if (field === 'album') {
    pipeline[0]['group']['picture'] = { $first: '$picture' };
  }
  // Sort
  if (sort) {
    pipeline[0]['sort'] = { _id: Number(sort) };
  }
  // Paginate
  if (page >= 0) {
    pipeline[0]['skip'] = (page - 1) * limit;
    pipeline[0]['limit'] = limit;
  }
  query
    .aggregate(pipeline)
    .then(response.success)
    .catch(response.error);
}

module.exports = {
  musicListOf: musicListOf
};
