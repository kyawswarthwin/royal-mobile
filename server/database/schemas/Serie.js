'use strict';

module.exports = {
  className: 'Serie',
  fields: {
    file: { type: 'String' },
    fileSize: { type: 'Number' },
    // title: { type: 'String' },
    artist: { type: 'Array' },
    // album: { type: 'String' },
    year: { type: 'String' },
    // track: { type: 'Number' },
    genre: { type: 'Array' },
    picture: { type: 'File' },
    synopsis: { type: 'String' },
    show: { type: 'String' },
    season: { type: 'Number' },
    episode: { type: 'Number' },
    episodeId: { type: 'String' },
    network: { type: 'String' }
  },
  indexes: {}
};
