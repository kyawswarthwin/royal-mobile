'use strict';

module.exports = {
  className: 'Application',
  fields: {
    file: { type: 'String' },
    fileSize: { type: 'Number' },
    icon: { type: 'File' },
    name: { type: 'String' },
    uniqueIdentifier: { type: 'String' },
    version: { type: 'String' },
    buildNumber: { type: 'String' },
    minOsVersion: { type: 'String' },
    deviceFamily: { type: 'Array' },
    platform: { type: 'String' }
  },
  indexes: {}
};
