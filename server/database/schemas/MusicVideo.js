'use strict';

module.exports = {
  className: 'MusicVideo',
  fields: {
    file: { type: 'String' },
    fileSize: { type: 'Number' },
    title: { type: 'String' },
    artist: { type: 'Array' },
    album: { type: 'String' },
    year: { type: 'String' },
    track: { type: 'Number' },
    genre: { type: 'Array' },
    picture: { type: 'File' }
  },
  indexes: {}
};
