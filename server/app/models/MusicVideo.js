'use strict';

class MusicVideo extends Parse.Object {
  constructor() {
    super('MusicVideo');
  }
}

module.exports = MusicVideo;
